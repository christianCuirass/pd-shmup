import "CoreLibs/graphics"
import "CoreLibs/object"
import "CoreLibs/sprites"
import "CoreLibs/timer"
import "bullet"

local pd <const> = playdate
local gfx <const> = pd.graphics

class('Actor').extends(gfx.sprite)

local homeX = 0
local homeY = 0
local pControlled = false

-- dpad vector
local inputX = 0
local inputY = 0

-- Position
local posX = 0
local posY = 0

function Actor:init (x, y, image, pCtrl)

    homeX = x
    homeY = y
    pControlled = pCtrl

    if image == nil
    then
        print("No Image")
    else
        self:setImage(image)
    end

    self.speed = 1
    self.modSpeed = 0
    self.defaultMaxBullets = 5
    self.homePos = ({x,y})
    self:moveTo(homeX, homeY)

end

function Actor:updatePosition (x, y)
    posX = x
    posY = y
end

function Actor:returnHome ()
    self:moveTo(homeX, homeY)
end

function Actor:getInputVectorX()
    return inputX
end
function Actor:getInputVectorY()
    return inputY
end

function Actor:Fire (aimX,aimY)
        local bulletInstance = Bullet(posX, posY, inputX, -inputY)
        bulletInstance:add()
end

function Actor:update()
    if pControlled then
        Actor.super.update()

        inputX = 0
        inputY = 0
        -- Directions
        if pd.buttonIsPressed(pd.kButtonLeft)then
            inputX = -1
            inputY = 0
        end
        if pd.buttonIsPressed(pd.kButtonRight)then
            inputX = 1
            inputY = 0
        end
        if pd.buttonIsPressed(pd.kButtonUp)then
            inputX = 0
            inputY = 1
        end
        if pd.buttonIsPressed(pd.kButtonDown)then
            inputX = 0
            inputY = -1
        end
        
        -- Diagonals
        if pd.buttonIsPressed(pd.kButtonLeft) and
        pd.buttonIsPressed(pd.kButtonUp)
        then
            inputX = -1
            inputY = 1
        end

        if pd.buttonIsPressed(pd.kButtonLeft) and
        pd.buttonIsPressed(pd.kButtonDown)
        then
            inputX = -1
            inputY = -1
        end
        if pd.buttonIsPressed(pd.kButtonRight) and
        pd.buttonIsPressed(pd.kButtonUp)
        then
            inputX = 1
            inputY = 1
        end
        if pd.buttonIsPressed(pd.kButtonRight) and
        pd.buttonIsPressed(pd.kButtonDown)
        then
            inputX = 1
            inputY = -1
        end

        if inputX == 0 then
            if inputY == 0 then
                return
            end
        end

        self:Fire(inputX, inputY)
    end
    
end