import "CoreLibs/graphics"
import "CoreLibs/object"
import "CoreLibs/sprites"
import "CoreLibs/timer"

local pd <const> = playdate
local gfx <const> = pd.graphics
local posX = 0
local posY = 0

local speed = 5

class('Bullet').extends(gfx.sprite)

function Bullet:init (initX, initY, traX, traY, image)
    posX = initX
    posY = initY
    self.trajX = traX
    self.trajY = traY

    self:moveTo(initX, initY)
    local image = gfx.image.new("images/bullet")
    self:setImage(image)
end

function Bullet:update()
    Bullet.super.update()
    self:moveBy(self.trajX * speed, self.trajY * speed)

end