import "CoreLibs/graphics"
import "CoreLibs/object"
import "CoreLibs/sprites"
import "CoreLibs/timer"
import "CoreLibs/crank"
import "actor"

local pd <const> = playdate
local gfx <const> = pd.graphics

-- images
local bulletImg = nil
local MAIN_SPRITE = nil
local playerActor = nil
local turret = nil
local bullet = nil

-- screen variables
local yMaxClamp = 220
local yMinClamp = 20
local turretYOffset = 10

-- game variables
local speed = 20
local nextVelocity = 0
local x = 200 -- mid screen
local y = 0

-- backgrounds
local BG1 = nil

local function init ()
    local loadImg = gfx.image.new("images/player_ship")
    MAIN_SPRITE = gfx.sprite.new(loadImg)
    local BG1Img = gfx.image.new("images/bg_1")
    local screwImg = gfx.image.new("images/bg_screw")
    local bgScrew = gfx.sprite.new(screwImg)

    local turretImg = gfx.image.new("images/player_turret")
    turret = gfx.sprite.new(turretImg)

    gfx.sprite.setBackgroundDrawingCallback(
        function (x,y,width,height)
            gfx.setClipRect(x,y,width,height)
            BG1Img:draw(0,0)
            gfx.clearClipRect()
        end
    )

    bgScrew:add()
    bgScrew:moveTo(200, 120)

    if MAIN_SPRITE == nil then
    else
        print("Created player actor")
        playerActor = Actor(x,120, loadImg, true)

        if playerActor == nill
        then
        else
            playerActor:add()
            playerActor:returnHome()
        end
    
    end 

    turret:add()
    turret:moveTo(x,120 + turretYOffset)
end

init()

function pd.update ()
    gfx.clear()

    -- Crank
    local change, acceleratedCHange = pd.getCrankChange()
    nextVelocity += change / 100

    if nextVelocity < 0 and y > yMinClamp then
        y += nextVelocity * speed
        playerActor:moveTo(x,y)
        turret:moveTo(x,y + turretYOffset)
    end

    if nextVelocity > 0 and y < yMaxClamp then
        y += nextVelocity * speed
        playerActor:moveTo(x,y)
        turret:moveTo(x,y +turretYOffset)
    end
    playerActor:updatePosition(x, y)
    gfx.sprite.update()

    nextVelocity = 0
    
end